#load "str.cma";;

exception MyExcept of string;;

type token= UNMINUS of int * int
	   | BINADD of int * int
	   | BINSUB of int * int
	   | BINDIV of int * int
	   | BINMUL of int * int
	   | BINMOD of int * int
	   | NEG of int * int
	   | AND of int * int
	   | OR of int * int
	   | ASSIGN of int * int
	   | EQ of int * int
	   | NE of int * int
	   | LT of int * int
	   | LTE of int * int
	   | GT of int * int
	   | GTE of int * int
	   | LP of int * int
	   | RP of int * int
	   | LB of int * int
	   | RB of int * int
	   | EOS of int * int
	   | COMMA of int * int
	   | INT of int * int
	   | BOOL of int * int
	   | IF of int * int
	   | THEN of int * int
	   | ELSE of int * int
	   | WHILE of int * int
	   | PROC of int * int
	   | PRINT of int * int
	   |  READ of int * int
	   | ERROR of int * int * int
	   | INTLIT of int * int * int
	   | IDENT of int * int * string
	   | BOOLVAL of int * int * bool ;;

  let toString (t : token) = match t with UNMINUS(a,b)    ->       "UNMINUS(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BINADD(a,b)    ->       "BINADD(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BINSUB(a,b)    ->       "BINSUB(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BINDIV(a,b)    ->       "BINDIV(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BINMUL(a,b)    ->       "BINMUL(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BINMOD(a,b)    ->       "BINMOD(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | NEG(a,b)    ->       "NEG(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | AND(a,b)    ->       "AND(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | OR(a,b)    ->       "OR(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | ASSIGN(a,b)    ->       "ASSIGN(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | EQ(a,b)    ->       "EQ(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | NE(a,b)    ->       "NE(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | LT(a,b)    ->       "LT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | LTE(a,b)    ->       "LTE(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | GT(a,b)    ->       "GT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | GTE(a,b)    ->       "GTE(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | LP(a,b)    ->       "LP(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | RP(a,b)    ->       "RP(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | LB(a,b)    ->       "LB(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | RB(a,b)    ->       "RB(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | EOS(a,b)    ->       "EOS(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | COMMA(a,b)    ->       "COMMA(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | INT(a,b)    ->       "INT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | BOOL(a,b)    ->   "BOOL(" ^ (string_of_int a)	^ "," ^ (string_of_int b)^ ")\n"
				     | IF(a,b)    ->       "IF(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | THEN(a,b)    ->       "THEN(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | ELSE(a,b)      ->       "ELSE(" ^ (string_of_int a) ^ ","^ (string_of_int b)^ ")\n"
				     | WHILE(a,b)    ->       "WHILE(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | PROC(a,b)    ->       "PROC(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | PRINT(a,b)    ->       "PRINT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | READ(a,b)    ->       "READ(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")\n"
				     | ERROR(a,b,c) -> "ERROR(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ "," ^ (string_of_int c) ^")\n"
				     | INTLIT(a,b,c) -> "INTLIT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ "," ^ (string_of_int c) ^")\n"
				     | IDENT(a,b,c) -> "IDENT(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ "," ^ c ^ ")\n"
				     | BOOLVAL(a,b,c) -> "BOOLVAL("^ (string_of_int a) ^ "," ^ (string_of_int b) ^ "," ^ (string_of_bool c) ^")\n"
  ;;
    

						    
let input_char_opt ic =
  try Some (input_char ic)
  with End_of_file -> None;;
 
let read_char ic =
  let rec aux acc i j=
    match input_char_opt ic with Some ch -> if ch <> '\n' 
                                            then aux ((ch,i,j)::acc) i (j+1)
                                            else aux acc (i+1) 1
    | None -> (List.rev acc)
  in
  aux [] 1 1;;
 
let chars_of_file filename =
  let ic = open_in filename in
  let char_list = read_char ic in
  close_in ic;
  (char_list);;

let int_reg = Str.regexp "[0-9]+$";;
let ident_reg = Str.regexp "[a-zA-Z][a-zA-Z0-9]*$";;
let recognise (str, a, b) = match str with "+" -> BINADD(a,b)
										| "~" -> UNMINUS(a,b)
										| "-" -> BINSUB(a,b)
										| "/" -> BINSUB(a,b)
										| "*" -> BINMUL(a,b)
										| "%" -> BINMOD(a,b)
										| "!" -> NEG(a,b)
										| "&&" -> AND(a,b)
										| "||" -> OR(a,b)
										| ":=" -> ASSIGN(a,b)
										| "=" -> EQ(a,b)
										| "<>" -> NE(a,b)
										| "<" -> LT(a,b)
										| "<=" ->LTE(a,b)
										| ">" -> GT(a,b)
										| ">=" -> GTE(a,b)
										| "(" -> LP(a,b)
										| ")" -> RP(a,b)
										| "{" -> LB(a,b)
										| "}" -> RB(a,b)
										| ";" -> EOS(a,b)
										| "," -> COMMA(a,b)
										| "int" -> INT(a,b)
										| "bool" -> BOOL(a,b)
										| "if" -> IF(a,b)
										| "then" -> THEN(a,b)
										| "else" -> ELSE(a,b)
										| "while" -> WHILE(a,b)
										| "proc" -> PROC(a,b)
										| "print" -> PRINT(a,b)
										| "read" -> READ(a,b)
										| "tt" -> BOOLVAL(a,b,true)
										| "ff" -> BOOLVAL(a,b,false)
										| _ -> if (Str.string_match int_reg str 0) 
												then INTLIT(a,b,(int_of_string str))
												else if (Str.string_match ident_reg str 0)
													then IDENT(a,b,str)
												else ERROR(a,b,(String.length str)) ;;


let get_ind1 (c,a,b) = a;;
let get_ind2 (c,a,b) = b;;
let get_indx1 ch_acc = match ch_acc with [] -> -1
  											| x::xs -> get_ind1 x;;
let get_indx2 ch_acc = match ch_acc with [] -> -1
  											| x::xs -> get_ind2 x;;

let get_sr (c,a,b) = String.make 1 c;;
let rec get_str ch_acc = match ch_acc with [] -> ""
  											| x::xs -> get_sr(x) ^ (get_str xs) ;;

let compress ch_acc = (get_str ch_acc, get_indx1 ch_acc,get_indx2 ch_acc);;

let reco1 ch_acc = recognise(compress (List.rev ch_acc));;

(* let singe_ops = ['+','~','-','/','*','%','!','=','(',')','{','}',';',','];;
let letters = ['a','b','c','d','e','f','g','h','i','j', 'k' ,'l' ,'m' ,'n' ,'o' ,'p' ,'q' ,'r' ,'s' ,'t' ,'u' ,'v' ,'w' ,'x' ,'y','z','A','B','C','D','E','F','G','H','I','J', 'K' ,'L' ,'M' ,'N' ,'O' ,'P' ,'Q' ,'R' ,'S' ,'T' ,'U' ,'V' ,'W' ,'X' ,'Y','Z' ];;
let digits = ['0' ,'1','2', '3', '4', '5', '6', '7', '8', '9'];; *)
let rec myfun tl acc ch_l =
	match ch_l with [] -> if (acc = []) then tl else (reco1 (acc))::tl
	| hd::tail ->
	(
		match hd with (' ',_,_) -> if (acc = []) then myfun tl [] tail
												else  myfun ((reco1 acc)::tl) [] tail
		| ('+',_,_) | ('~',_,_) | ('-',_,_) | ('/',_,_) | ('*',_,_) | ('%',_,_) | ('!',_,_) | ('=',_,_) | ('(',_,_) | (')',_,_) | ('{',_,_) | ('}',_,_) | (';',_,_) | (',',_,_)  ->
							if (acc=[]) then (myfun ((reco1 (hd::[]))::tl) [] tail )
										else (myfun ((reco1 (hd::[]))::(reco1 (acc))::tl) [] tail) 
		| ('&',_,_) -> (match tail with [] -> (reco1 (hd::acc))::tl
										| x::xs -> (match x with ('&',_,_) -> if (acc<>[]) then myfun ((reco1(x::hd::[]))::(reco1(acc))::tl) [] xs
																							else myfun ((reco1(x::hd::[]))::tl) [] xs
																| _ -> myfun tl (hd::acc) tail  
													)
						)
		| ('|',_,_) -> (match tail with [] -> (reco1 (hd::acc))::tl
										| x::xs -> (match x with ('|',_,_) -> if (acc<>[]) then myfun ((reco1(x::hd::[]))::(reco1(acc))::tl) [] xs
																							else myfun ((reco1(x::hd::[]))::tl) [] xs
																| _ -> myfun tl (hd::acc) tail  
													)
						)
		| (':',_,_) -> (match tail with [] -> (reco1 (hd::acc))::tl
										| x::xs -> (match x with ('=',_,_) -> if (acc<>[]) then myfun ((reco1(x::hd::[]))::(reco1(acc))::tl) [] xs
																							else myfun ((reco1(x::hd::[]))::tl) [] xs
																| _ -> myfun tl (hd::acc) tail  
													)
						)
		| ('<',_,_) -> (match tail with [] -> if (acc = []) then (reco1 (hd::[]))::tl
																else (reco1 (hd::[]))::(reco1 (acc))::tl
										| x::xs -> (match x with ('=',_,_) | ('>',_,_) -> if (acc<>[]) then myfun ((reco1(x::hd::[]))::(reco1(acc))::tl) [] xs
																							else myfun ((reco1(x::hd::[]))::tl) [] xs
																| _ ->  if (acc=[]) then (myfun ((reco1 (hd::[]))::tl) [] tail )
																					else (myfun ((reco1 (hd::[]))::(reco1 (acc))::tl) [] tail)  
													)
						)
		| ('>',_,_) -> (match tail with [] -> if (acc = []) then (reco1 (hd::[]))::tl
																else (reco1 (hd::[]))::(reco1 (acc))::tl
										| x::xs -> (match x with ('=',_,_) -> if (acc<>[]) then myfun ((reco1(x::hd::[]))::(reco1(acc))::tl) [] xs
																							else myfun ((reco1(x::hd::[]))::tl) [] xs
																| _ ->  if (acc=[]) then (myfun ((reco1 (hd::[]))::tl) [] tail )
																					else (myfun ((reco1 (hd::[]))::(reco1 (acc))::tl) [] tail)  
													)
						)
		| _ -> myfun tl (hd::acc) tail
 	);;




